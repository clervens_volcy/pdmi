package com.volcy.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

public class LocationTracker extends Service implements LocationListener {
	private final Context mContext;
	private LocationManager locationManager;
	private LocationChangedListener callback;

	// Flags
	private boolean isGPSEnabled;
	private boolean isNetworkEnabled;
	private boolean canGetLocation;

	// Location updates data
	private final int MIN_DISTANCE; // En metre => 5m
	private final int MIN_TIME; // En millisecondes => 1min

	// Location data
	private Location location;
	private double latitude;
	private double longitude;

	public LocationTracker(Context context) {
		mContext = context;

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		MIN_DISTANCE = Integer.valueOf(prefs.getString("min_distance", "5"));
		MIN_TIME = 1000 * Integer.valueOf(prefs.getString("min_time", "15"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String result = "LocationTracker\n";
		result += "lat : " + this.getLatitude() + ", lon : "
				+ this.getLongitude() + "\n";
		result += "UPDATE LOCATION'S TRIGGER\n";
		result += "MIN_DISTANCE : " + this.MIN_DISTANCE + ", MIN_TIME : "
				+ this.MIN_TIME;

		return result;
	}

	/*
	 * Getter pour la latitude.
	 */
	public double getLatitude() {
		if (location != null) {
			latitude = location.getLatitude();
		}
		return latitude;
	}

	/*
	 * Getter pour la longitude.
	 */
	public double getLongitude() {
		if (location != null) {
			longitude = location.getLongitude();
		}
		return longitude;
	}

	public boolean canGetLocation() {
		return this.canGetLocation;
	}

	public Location startTracking() {
		if (locationManager == null) {
			locationManager = (LocationManager) mContext
					.getSystemService(LOCATION_SERVICE);
		}
		return LocationUpdate();
	}

	private Location LocationUpdate() {

		try {
			// getting GPS status
			isGPSEnabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);

//			// getting network status
//			isNetworkEnabled = locationManager
//					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			Log.d(LocationTracker.class.toString(), "Providers status GPS : "
					+ isGPSEnabled + ", NETWORK : " + isNetworkEnabled);

			if (!isGPSEnabled && !isNetworkEnabled) {
				canGetLocation = false;
			} else {
				canGetLocation = true;

				if (callback != null) {
					// OnLocationTracking's Trigger
					callback.onLocationTracking();
				}

				// if GPS Enabled get lat/long using GPS Services
				if (isGPSEnabled) {
					locationManager.requestLocationUpdates(
							LocationManager.GPS_PROVIDER, MIN_TIME,
							MIN_DISTANCE, this);
					Log.d(LocationTracker.class.toString(), "GPS Enabled");
					if (locationManager != null) {
						location = locationManager
								.getLastKnownLocation(LocationManager.GPS_PROVIDER);
						if (location != null) {
							latitude = location.getLatitude();
							longitude = location.getLongitude();
						}

						Log.d(LocationTracker.class.toString(), "Accuracy : " + location.getAccuracy());
					}
				}

//				// get location from Network Provider [Less accurate]
//				if (isNetworkEnabled) {
//					locationManager.requestLocationUpdates(
//							LocationManager.NETWORK_PROVIDER, MIN_TIME,
//							MIN_DISTANCE, this);
//					Log.d(LocationTracker.class.toString(), "Network Enabled");
//					if (locationManager != null) {
//						if (!isGPSEnabled) {
//							location = locationManager
//									.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//						} else {
//							if (location != null
//									&& location.getAccuracy() > 200)
//								location = locationManager
//										.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//						}
//					}
//				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return location;
	}

	public void stopLocationUpdate() {
		if (locationManager != null) {
			locationManager.removeUpdates(this);
		}
	}

	public void setOnLocationChangedListener(LocationChangedListener listener) {
		callback = listener;
	}

	@Override
	public void onLocationChanged(Location location) {
		if (callback != null) {
			// OnLocationChanged's Trigger
			callback.onLocationChanged(location);
			LocationUpdate();
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	public interface LocationChangedListener {
		public void onLocationTracking();

		public void onLocationChanged(Location location);
	}

}
