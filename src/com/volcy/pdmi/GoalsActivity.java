package com.volcy.pdmi;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.volcy.db.PdmiDBAdapter;
import com.volcy.db.BLL.PdmiSession;

public class GoalsActivity extends BaseActivity implements OnClickListener {
	private TextView tvTotalSteps, tvTotalCalories;
	private EditText etGoalSteps, etGoalCalories;
	private Button btnOk, btnAnnuler;
	private LinearLayout llTSteps, llTCalories;

	private SharedPreferences sharedPref;

	private int totaux_steps, totaux_calories, totaux_duration;
	private int goal_steps, goal_calories;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_goals);

		initialize();
	}

	private void initialize() {
		testDbSession();

		tvTotalSteps = (TextView) findViewById(R.id.tvGoalsTotalStepsResult);
		tvTotalCalories = (TextView) findViewById(R.id.tvGoalsTotalCaloriesResult);
		etGoalSteps = (EditText) findViewById(R.id.etGoalsSteps);
		etGoalCalories = (EditText) findViewById(R.id.etGoalsCalories);

		llTSteps = (LinearLayout) findViewById(R.id.llGoalsTotalSteps);
		llTCalories = (LinearLayout) findViewById(R.id.llGoalsTotalCalories);

		btnOk = (Button) findViewById(R.id.btnGoalsOK);
		btnAnnuler = (Button) findViewById(R.id.btnGoalsCancel);
		btnOk.setOnClickListener(this);
		btnAnnuler.setOnClickListener(this);

		sharedPref = getSharedPreferences(getString(R.string.app_name),
				Context.MODE_PRIVATE);

		totaux_steps = sharedPref.getInt(getString(R.string.t_steps), 0);
		totaux_calories = sharedPref.getInt(getString(R.string.t_calories), 0);
		totaux_duration = sharedPref.getInt(getString(R.string.t_duration), 0);

		goal_steps = sharedPref.getInt(getString(R.string.g_steps), 0);
		etGoalSteps.setText(String.valueOf(goal_steps));
		goal_calories = sharedPref.getInt(getString(R.string.g_calories), 0);
		etGoalCalories.setText(String.valueOf(goal_calories));

		setTotauxVisibility();
	}

	private void testDbSession() {

		PdmiDBAdapter db = new PdmiDBAdapter(this);
		db.open();
		ArrayList<PdmiSession> sessions = db.getSessions();
		db.close();
		String t = "";
		if (sessions != null) {
			for (int i = 0; i < sessions.size(); i++) {
				t = t + "[" + sessions.get(i).get_id() + " - "
						+ sessions.get(i).get_calories() + "], ";
			}
			Toast.makeText(this, t, Toast.LENGTH_LONG).show();
		}
	}

	// But: Affiche les Totaux s'ils existent
	private void setTotauxVisibility() {

		if (totaux_steps != 0) {
			tvTotalSteps.setText(String.valueOf(totaux_steps));
		} else {
			llTSteps.setVisibility(View.GONE);
		}

		if (totaux_calories != 0) {
			tvTotalCalories.setText(String.valueOf(totaux_calories));
		} else {
			llTCalories.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnGoalsOK:

			// But: mettre � jour les objectifs de l'application
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.goal_dialog_title)
					.setMessage(R.string.goal_dialog_message)
					.setPositiveButton(R.string.ok,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									SharedPreferences.Editor editor = sharedPref
											.edit();

									editor.putInt(getString(R.string.g_steps),
											Integer.parseInt(etGoalSteps
													.getText().toString()));
									editor.putInt(
											getString(R.string.g_calories),
											Integer.parseInt(etGoalCalories
													.getText().toString()));
									editor.commit();

									Toast.makeText(GoalsActivity.this,
											R.string.goal_dialog_success,
											Toast.LENGTH_SHORT).show();
								}
							})
					.setNegativeButton(R.string.cancel,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
								}
							});

			// But: Affichage de la fen�tre de dialogue
			AlertDialog alertDialog = builder.create();
			alertDialog.show();
			break;
		case R.id.btnGoalsCancel:
			// Reset des valuers des EditTexts
			etGoalSteps.setText(String.valueOf(goal_steps));
			etGoalCalories.setText(String.valueOf(goal_calories));

			// Reset des valeurs dans les sharedPreferences.
			SharedPreferences.Editor editor = sharedPref.edit();

			editor.putInt(getString(R.string.g_steps),
					Integer.parseInt(etGoalSteps.getText().toString()));
			editor.putInt(getString(R.string.g_calories),
					Integer.parseInt(etGoalCalories.getText().toString()));
			editor.commit();
			Toast.makeText(this, R.string.goal_reset, Toast.LENGTH_SHORT).show();
			break;
		}
	}

}
