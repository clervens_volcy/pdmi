package com.volcy.pdmi;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class ProfileActivity extends BaseActivity implements OnClickListener {
	private RadioGroup rgSexe;
	private RadioButton rbtnHomme, rbtnFemme;
	private EditText etNom, etPoids;
	private Button btnOK, btnAnnuler;

	private SharedPreferences sharedPref;

	// Profile preferences
	private boolean estUnHomme;
	private String nom;
	private double poids;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);

		initialize();
	}

	private void initialize() {
		sharedPref = getSharedPreferences(getString(R.string.app_name),
				Context.MODE_PRIVATE);

		profilePreferences();

		rgSexe = (RadioGroup) findViewById(R.id.rgProfileSexe);
		rbtnHomme = (RadioButton) findViewById(R.id.rbtnProfileMale);
		rbtnFemme = (RadioButton) findViewById(R.id.rbtnProfileFemale);
		etNom = (EditText) findViewById(R.id.etProfileNom);
		etPoids = (EditText) findViewById(R.id.etProfilePoids);
		btnOK = (Button) findViewById(R.id.btnProfileOK);
		btnAnnuler = (Button) findViewById(R.id.btnProfileCancel);

		if (estUnHomme) {
			rbtnHomme.setChecked(true);
		} else {
			rbtnFemme.setChecked(true);
		}

		etNom.setText(nom);
		etPoids.setText(String.valueOf(poids));

		btnOK.setOnClickListener(this);
		btnAnnuler.setOnClickListener(this);
	}

	// But: initialize les variables des preferences du profile.
	private void profilePreferences() {
		estUnHomme = sharedPref.getBoolean(getString(R.string.p_estUnHomme),
				true);
		nom = sharedPref.getString(getString(R.string.p_nom), "");
		poids = sharedPref.getFloat(getString(R.string.p_poids), (float) 0.0);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnProfileOK:
			final boolean estUnHommeResult = (rgSexe.getCheckedRadioButtonId() == R.id.rbtnProfileMale);

			// But: mettre � jour les infos du profile
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.goal_dialog_title)
					.setMessage(R.string.goal_dialog_message)
					.setPositiveButton(R.string.ok,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									SharedPreferences.Editor editor = sharedPref
											.edit();

									editor.putBoolean(
											getString(R.string.p_estUnHomme),
											estUnHommeResult);
									editor.putString(getString(R.string.p_nom),
											etNom.getText().toString());
									editor.putFloat(
											getString(R.string.p_poids), Float
													.parseFloat(etPoids.getText()
															.toString()));
									editor.commit();

									Toast.makeText(ProfileActivity.this,
											R.string.goal_dialog_success,
											Toast.LENGTH_SHORT).show();
								}
							})
					.setNegativeButton(R.string.cancel,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
								}
							});

			// But: Affichage de la fen�tre de dialogue
			AlertDialog alertDialog = builder.create();
			alertDialog.show();
			break;
		case R.id.btnProfileCancel:
			// Reset des valuers des EditTexts
			etNom.setText(nom);
			etPoids.setText(String.valueOf(poids));

			// Reset des valeurs dans les sharedPreferences.
			SharedPreferences.Editor editor = sharedPref.edit();

			editor.putBoolean(getString(R.string.p_estUnHomme), estUnHomme);
			editor.putString(getString(R.string.p_nom), etNom.getText()
					.toString());
			editor.putFloat(getString(R.string.p_poids),
					Float.valueOf(etPoids.getText().toString()));
			editor.commit();
			Toast.makeText(this, R.string.goal_reset, Toast.LENGTH_SHORT)
					.show();

			break;
		}

	}
}
