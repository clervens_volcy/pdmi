package com.volcy.pdmi;

import android.app.ActionBar;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.volcy.managers.PdmiManager;
import com.volcy.services.LocationTracker;

public class MainActivity extends BaseActivity implements OnClickListener,
		LocationTracker.LocationChangedListener {
	Button btnTracking, btnPause, btnStop;
	TextView tvLatitude, tvLongitude, tvSpeed, tvSteps, tvDistance,
			tvTrackLoading, tvCalories;
	ProgressBar pbTrackLoading;
	LinearLayout layoutPauseStop;

	LocationTracker lTracker;
	PdmiManager pdmi;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initialize();
	}

	private void initialize() {
		btnTracking = (Button) findViewById(R.id.btnMainStartTracking);
		btnPause = (Button) findViewById(R.id.btnMainPauseTracking);
		btnStop = (Button) findViewById(R.id.btnMainStopTracking);

		tvLatitude = (TextView) findViewById(R.id.tvMainLatitude);
		tvLongitude = (TextView) findViewById(R.id.tvMainLongitude);
		tvSpeed = (TextView) findViewById(R.id.tvMainSpeedResult);
		tvDistance = (TextView) findViewById(R.id.tvMainDistanceResult);
		tvSteps = (TextView) findViewById(R.id.tvMainStepsResult);
		tvTrackLoading = (TextView) findViewById(R.id.tvMainTrackLoading);
		tvCalories = (TextView) findViewById(R.id.tvMainCaloriesResult);

		pbTrackLoading = (ProgressBar) findViewById(R.id.pbMainTrackLoading);

		layoutPauseStop = (LinearLayout) findViewById(R.id.llMainPauseStop);

		btnTracking.setOnClickListener(this);
		btnPause.setOnClickListener(this);
		btnStop.setOnClickListener(this);

		lTracker = new LocationTracker(this);
		lTracker.setOnLocationChangedListener(this);

		pdmi = new PdmiManager(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnMainStartTracking:

			lTracker.startTracking();

			tvTrackLoading.setVisibility(View.VISIBLE);
			pbTrackLoading.setVisibility(View.VISIBLE);

			btnTracking.setVisibility(View.GONE);
			layoutPauseStop.setVisibility(View.VISIBLE);
			break;
		case R.id.btnMainPauseTracking:
			// test
			pdmi.showObjectifDialog("JP and Cler are in love with chixx");
			// end test
			lTracker.stopLocationUpdate();

			tvTrackLoading.setVisibility(View.GONE);
			pbTrackLoading.setVisibility(View.GONE);

			btnTracking.setVisibility(View.VISIBLE);
			layoutPauseStop.setVisibility(View.GONE);
			break;
		case R.id.btnMainStopTracking:
			lTracker.stopLocationUpdate();
			pdmi.saveSession();

			pdmi = new PdmiManager(this);

			btnTracking.setVisibility(View.VISIBLE);
			layoutPauseStop.setVisibility(View.GONE);
			break;
		}
	}

	@Override
	public void onLocationTracking() {

	}

	@Override
	public void onLocationChanged(Location location) {
		if (tvTrackLoading.getVisibility() != View.GONE) {
			tvTrackLoading.setVisibility(View.GONE);
			pbTrackLoading.setVisibility(View.GONE);
		}

		pdmi.addLocation(location);

		tvLatitude.setText("Latitude : " + location.getLatitude());
		tvLongitude.setText("Longitude : " + location.getLongitude());
		tvSpeed.setText(String.valueOf((Math.round(pdmi.getSpeed() * 100.0) / 100.0)));
		tvDistance
				.setText(String.valueOf((Math.round(pdmi.getTotalDistance() * 100.0) / 100.0)));
		tvSteps.setText(String.valueOf(pdmi.getSteps()));
		tvCalories.setText(String.valueOf(pdmi.getTotalCalories()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onStop()
	 */
	@Override
	protected void onStop() {
		super.onStop();
		lTracker.stopLocationUpdate();
	}

}
