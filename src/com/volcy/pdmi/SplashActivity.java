package com.volcy.pdmi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

public class SplashActivity extends Activity {
	private TextView tvProgrammers, tvNotices;
	private ProgressBar pbWaiting;
	private Context mContext;

	private boolean canStart = true;
	private MediaPlayer ourFirstSong;

	private Intent intent;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		initialize();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onPause() {
		super.onPause();
		ourFirstSong.release();
		if (canStart) {
			if (intent == null)
				intent = new Intent(this, HomeActivity.class);
			startActivity(intent);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		canStart = true;
		initialize();
	}

	public void initialize() {
		mContext = this.getApplicationContext();

		ourFirstSong = MediaPlayer.create(mContext, R.raw.son_splash);
		ourFirstSong.start();

		tvProgrammers = (TextView) findViewById(R.id.tvSplashProgrammers);
		tvNotices = (TextView) findViewById(R.id.tvSplashNotices);
		tvNotices.setText("");
		pbWaiting = (ProgressBar) findViewById(R.id.pbSplashWaiting);

		new SplashTask().execute(5);
	}

	class SplashTask extends AsyncTask<Integer, String, Void> {
		private Integer cycle = 0;

		@Override
		protected Void doInBackground(Integer... params) {

			while (cycle <= params[0]) {
				try {
					Thread.sleep(500);
					switch (cycle) {
					case 0:
						this.publishProgress("Checking GPS status.");

						break;
					case 1:
						publishProgress("Check profile's preferences");

						break;
					}
					cycle++;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				if (cycle == params[0] && !canStart)
					cycle = 0;
			}

			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
			tvNotices.setText(values[0]);
			Log.d(SplashActivity.class.toString(), "Cycle : " + cycle);
			switch (cycle) {
			case 1:
				if (canStart) {
					LocationManager locationManager = (LocationManager) mContext
							.getSystemService(LOCATION_SERVICE);

					boolean isGPSEnabled = locationManager
							.isProviderEnabled(LocationManager.GPS_PROVIDER);

					if (!isGPSEnabled) {
						canStart = false;
						new AlertDialog.Builder(SplashActivity.this)
								.setMessage("GPS required.")
								.setCancelable(false)
								.setPositiveButton("GPS",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												SplashActivity.this
														.startActivity(new Intent(
																Settings.ACTION_LOCATION_SOURCE_SETTINGS));
											}
										})
								.setNegativeButton("Exit",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												dialog.cancel();
												SplashActivity.this.finish();
											}
										}).create().show();
						this.cancel(true);
					}
				}
				break;
			case 2:
				SharedPreferences sharedPref = getSharedPreferences(
						getString(R.string.app_name), Context.MODE_PRIVATE);
				Boolean pDemarrage = sharedPref.getBoolean("permier_demarrage",
						true);
				if (pDemarrage) {
					intent = new Intent(SplashActivity.this,
							ProfileActivity.class);
					SharedPreferences.Editor editor = sharedPref.edit();
					editor.putBoolean("permier_demarrage", false);
					editor.commit();
				}

				break;
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (canStart)
				SplashActivity.this.finish();
		}

	}

}
