package com.volcy.pdmi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class HomeActivity extends Activity implements OnClickListener {
	
	private Button btnTracking;
	private Button btnGoals;
	private Button btnStats;
	private Button btnProfile;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_home);
		initialize();
	}

	private void initialize() {
		btnTracking = (Button) findViewById(R.id.btnHomeTracking);
		btnTracking.setOnClickListener(this);
		
		btnGoals = (Button) findViewById(R.id.btnHomeGoals);
		btnGoals.setOnClickListener(this);
		
		btnStats = (Button) findViewById(R.id.btnHomeStats);
		btnStats.setOnClickListener(this);
		
		btnProfile = (Button) findViewById(R.id.btnHomeProfile);
		btnProfile.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Intent i;
		switch(v.getId()){
		case R.id.btnHomeTracking:
			i = new Intent(this, MainActivity.class);
			startActivity(i);
			
			break;
			
		case R.id.btnHomeGoals:
			i = new Intent(this, GoalsActivity.class);
			startActivity(i);
			
			break;
			
		case R.id.btnHomeStats:
			i = new Intent(this, TestGraphViewActivity.class);
			startActivity(i);
			
			break;
			
		case R.id.btnHomeProfile:
			i = new Intent(this, ProfileActivity.class);
			startActivity(i);
			
			break;
		}
	}

}
