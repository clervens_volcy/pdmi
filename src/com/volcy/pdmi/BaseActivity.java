package com.volcy.pdmi;

import java.util.Date;

import com.volcy.db.BLL.PdmiSession;
import com.volcy.managers.PdmiManager;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class BaseActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// But:Permet d'int�ragir avec l'icone du menu
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */

	// But: Cr�ation de la navigation des menu et de l'icone dans le haut de
	// l'application
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent i;

		switch (item.getItemId()) {
		case R.id.menu_settings:
			i = new Intent(this, Preferences.class);
			startActivity(i);

			break;
		case R.id.menu_stats:
			i = new Intent(this, TestGraphViewActivity.class);
			startActivity(i);

			break;
		case R.id.menu_goals:
			i = new Intent(this, GoalsActivity.class);
			startActivity(i);

			break;

		case R.id.menu_profile:
			i = new Intent(this, ProfileActivity.class);
			startActivity(i);

			break;
		case R.id.menu_add_session:
			PdmiManager pdmi = new PdmiManager(this);

			PdmiSession s = new PdmiSession((new Date()).getTime(),
					(int) (Math.random() * 3000), (int) (Math.random() * 300),
					(int) (Math.random() * 1000),
					(3600 + (int) (Math.random() * (7200 - 3600))));

			if (pdmi.debugSaveSession(s))
				Toast.makeText(this, "Debug : ajout d'une session!", Toast.LENGTH_LONG).show();

			break;
		// But:Redirige l'application vers la page d'acceuil lorsque l'icone
		// subit un click
		case android.R.id.home:
			i = new Intent(this, HomeActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

		return false;
	}

}