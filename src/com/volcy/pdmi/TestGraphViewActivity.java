package com.volcy.pdmi;

import java.util.ArrayList;

import android.app.ActionBar;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.GraphViewSeries.GraphViewStyle;
import com.jjoe64.graphview.LineGraphView;
import com.volcy.db.PdmiDBAdapter;
import com.volcy.db.BLL.PdmiSession;

public class TestGraphViewActivity extends BaseActivity implements
		OnCheckedChangeListener {

	private CheckBox chkCalories, chkDistance, chkDuration;
	private LinearLayout graphLayout;

	private ArrayList<PdmiSession> sessions;

	private GraphViewData[] data;
	private GraphView graphView;

	// Donn�es pour le graphique
	private GraphViewSeries caloriesData;
	private GraphViewSeries distanceData;
	private GraphViewSeries durationData;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_test_graph);

		initialize();

		setChart();
	}

	private void initialize() {

		PdmiDBAdapter db = new PdmiDBAdapter(this);
		db.open();
		// on r�cup�re les sessions de la bd sqlite.
		sessions = db.getSessions();
		if (sessions == null)
			sessions = new ArrayList<PdmiSession>();
		db.close();

		graphLayout = (LinearLayout) findViewById(R.id.testGraphLayout);

		chkCalories = (CheckBox) findViewById(R.id.chkboxTestGraphCalories);
		chkDistance = (CheckBox) findViewById(R.id.chkboxTestGraphDistance);
		chkDuration = (CheckBox) findViewById(R.id.chkboxTestGraphDuration);

		chkCalories.setOnCheckedChangeListener(this);
		chkDistance.setOnCheckedChangeListener(this);
		chkDuration.setOnCheckedChangeListener(this);
	}

	// But: Affiche le graphique.
	private void setChart() {
		data = new GraphViewData[sessions.size()];
		
		// creation du graphique
		graphView = new LineGraphView(this, getString(R.string.app_name));

		if (sessions.size() > 0) {
			graphLayout.removeAllViews();

			if (chkCalories.isChecked()) {
				ajouterDataCalories();
			}

			if (chkDistance.isChecked()) {
				ajouterDataDistance();
			}

			if (chkDuration.isChecked()) {
				ajouterDataDuration();
			}

			graphView.setScalable(true);

			// Afficher la l�gende si l'un des �l�ments est s�lectionn�
			if (chkCalories.isChecked() || chkDistance.isChecked()
					|| chkDuration.isChecked()) {
				graphView.setShowLegend(true);
			}
			int viewPortSize = 5;
			if (sessions.size() < 5)
				viewPortSize = sessions.size();

			graphView.setViewPort(0, viewPortSize);
			// Active le zoom et le scroll
			graphView.setScrollable(true);
			graphView.setScalable(true);
		}
		
		// On ajoute les donn�es disponible au layout
		graphLayout.addView(graphView);
	}

	@Override
	public void onCheckedChanged(CompoundButton item, boolean isChecked) {
		switch (item.getId()) {
		case R.id.chkboxTestGraphCalories:
		case R.id.chkboxTestGraphDistance:
		case R.id.chkboxTestGraphDuration:
			setChart();
			break;
		}
	}

	private void ajouterDataCalories() {
		// Ne pas refaire de boucle si les donn�es sont d�j� dans la variable
		if (caloriesData == null) {

			for (int i = 0; i < sessions.size(); i++) {
				data[i] = new GraphViewData(i, sessions.get(i).get_calories());
			}
			caloriesData = new GraphViewSeries("Calories", new GraphViewStyle(
					Color.rgb(200, 50, 00), 0), data);
		}
		graphView.addSeries(caloriesData);
	}

	private void ajouterDataDistance() {
		if (distanceData == null) {

			for (int i = 0; i < sessions.size(); i++) {
				data[i] = new GraphViewData(i, sessions.get(i).get_distance());
			}
			distanceData = new GraphViewSeries("Distance", new GraphViewStyle(
					Color.rgb(90, 250, 00), 0), data);
		}
		graphView.addSeries(distanceData);
	}

	private void ajouterDataDuration() {
		if (durationData == null) {

			for (int i = 0; i < sessions.size(); i++) {
				data[i] = new GraphViewData(i, sessions.get(i).get_duration());
			}
			durationData = new GraphViewSeries("Duration", null, data);
		}
		graphView.addSeries(durationData);
	}
}
