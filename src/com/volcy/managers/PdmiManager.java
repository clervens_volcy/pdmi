package com.volcy.managers;

import java.util.ArrayList;
import java.util.Date;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.location.Location;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.util.Log;

import com.volcy.db.PdmiDBAdapter;
import com.volcy.db.BLL.PdmiSession;
import com.volcy.pdmi.R;

public class PdmiManager {
	private final int STEP_LENGTH;

	private Context mContext;

	private ArrayList<Location> locations;
	private double totalDistance, lastDistance, speed, calories = 0;
	private double steps;
	private PdmiSession session;

	private boolean estUnHomme;
	private float poids;

	private int stepsRestant, caloriesRestant;

	public PdmiManager(Context context) {
		mContext = context;

		SharedPreferences prefs = mContext.getSharedPreferences(
				mContext.getString(R.string.app_name), Context.MODE_PRIVATE);

		STEP_LENGTH = Integer.valueOf(prefs.getString("step_length", "50"));
		estUnHomme = prefs.getBoolean(
				mContext.getString(R.string.p_estUnHomme), true);
		poids = prefs.getFloat(mContext.getString(R.string.p_poids),
				(float) 0.0);

		locations = new ArrayList<Location>();
		totalDistance = lastDistance = speed = 0;
		steps = 0;

		session = new PdmiSession();
		setObjectifsValues();
	}

	public Double addLocation(Location loc) {
		if (locations.size() > 0) {
			lastDistance = locations.get(locations.size() - 1).distanceTo(loc);

			calculateSpeed(loc);
			calculateSteps();

			totalDistance += lastDistance;

			locations.add(loc);
			return lastDistance;
		}

		locations.add(loc);
		return 0.0;
	}

	public int getSteps() {
		return (int) steps;
	}

	public double getSpeed() {
		return speed;
	}

	public double getTotalDistance() {
		return totalDistance;
	}

	public int getTotalCalories() {
		return (int) calories;
	}

	private void calculateSpeed(Location loc) {
		long elapsedTime = (loc.getTime() - locations.get(locations.size() - 1)
				.getTime()) / 1000;
		speed = lastDistance / elapsedTime;

		calculateCalories((elapsedTime / 60.0), speed);
	}

	// But: Calcule de nombre de pas effectu� par l'utilisateur
	private void calculateSteps() {
		steps += (lastDistance * 100) / STEP_LENGTH;
		verifierEtatStepsObjectif((int) steps);
	}

	// But: Calcule le nombre de calories brul�es
	private void calculateCalories(double minutes, double vitesse) {

		double facteurPoids = poids * 2.204;
		double facteur;

		// 4km/h
		if (vitesse <= 1.11) {
			if (estUnHomme) {
				facteur = 0.0188;
			} else {
				facteur = 0.0178;
			}
			// 6km/h
		} else if (vitesse <= 1.66) {
			if (estUnHomme) {
				facteur = 0.0292;
			} else {
				facteur = 0.0277;
			}
			// � partir d'ici vitesse de jogging
			// 10km/h
		} else if (vitesse <= 2.77) {
			if (estUnHomme) {
				facteur = 0.0786;
			} else {
				facteur = 0.075;
			}
			// 12.5km/h
		} else if (vitesse <= 3.47) {
			if (estUnHomme) {
				facteur = 0.0968;
			} else {
				facteur = 0.0924;
			}
			// 15 km/h
		} else if (vitesse <= 4.16) {
			if (estUnHomme) {
				facteur = 0.11495;
			} else {
				facteur = 0.1097;
			}
			// 20km/h
		} else if (vitesse <= 5.55) {
			if (estUnHomme) {
				facteur = 0.2238;
			} else {
				facteur = 0.2136;
			}
			// Impossible
		} else {
			facteur = 0;
		}

		calories = calories + (facteurPoids * minutes * facteur);
		Log.i(PdmiManager.class.toString(), calories + " : " + facteurPoids
				+ ", " + minutes + ", " + facteur);

		verifierEtatCaloriesObjectif((int) calories);

	}

	// But: savegarde dans la bd sqlite la session pdmi.
	public Boolean saveSession() {
		Date now = new Date();
		int duration = (int) (now.getTime() - locations.get(0).getTime());
		session = new PdmiSession(now.getTime(), totalDistance, (int) calories,
				(int) steps, duration);

		PdmiDBAdapter db = new PdmiDBAdapter(mContext);
		db.open();
		boolean result = (db.insertSession(session) != -1);
		db.close();

		updateTotauxObjectifs();

		return result;
	}

	public Boolean debugSaveSession(PdmiSession session) {

		PdmiDBAdapter db = new PdmiDBAdapter(mContext);
		db.open();
		boolean result = (db.insertSession(session) != -1);
		db.close();
		
		steps = session.get_steps();
		calories = session.get_calories();
		
		updateTotauxObjectifs();

		return result;
	}

	// But: m�thode pour mettre � jour les totaux pour les objectifs
	private void updateTotauxObjectifs() {
		SharedPreferences sharedPref = mContext.getSharedPreferences(
				mContext.getString(R.string.app_name), Context.MODE_PRIVATE);

		int totaux_steps = sharedPref.getInt(
				mContext.getString(R.string.t_steps), 0)
				+ (int) steps;
		int totaux_calories = sharedPref.getInt(
				mContext.getString(R.string.t_calories), 0)
				+ (int) calories;
		int totaux_duration = sharedPref.getInt(
				mContext.getString(R.string.t_duration), 0)
				+ session.get_duration();

		SharedPreferences.Editor editor = sharedPref.edit();

		editor.putInt(mContext.getString(R.string.t_steps), totaux_steps);
		editor.putInt(mContext.getString(R.string.t_calories), totaux_calories);
		editor.putInt(mContext.getString(R.string.t_duration), totaux_duration);

		editor.commit();
	}

	// But: Afficher une fen�tre de dialogue.
	public void showObjectifDialog(String message) {

		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setTitle("Congradulations"); // changer en string plus tard
												// (localisation)
		builder.setMessage(message);
		builder.setPositiveButton(R.string.ok,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// logique d'application

					}
				});

		// But: jouer un son lors de la compl�tion de l'objectif
		MediaPlayer mp = MediaPlayer.create(mContext, R.raw.sound_dialog);
		mp.start();

		// But: Affichage de la fen�tre de dialogue
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}

	// But: v�rifie si l'objectif de steps est atteint, si oui ouvre une boite
	// de dialogue.
	private void verifierEtatStepsObjectif(int steps) {
		if (stepsRestant != 0 && steps >= stepsRestant) {
			stepsRestant = 0;
			showObjectifDialog("Hey steps");
		}
	}

	// But: v�rifie si l'objectif de calories est atteint, si oui ouvre une
	// boite de dialogue.
	private void verifierEtatCaloriesObjectif(int calories) {
		if (caloriesRestant != 0 && calories >= caloriesRestant) {
			caloriesRestant = 0;
			showObjectifDialog("Hey calories");
		}
	}

	private void setObjectifsValues() {
		SharedPreferences sharedPref = mContext.getSharedPreferences(
				mContext.getString(R.string.app_name), Context.MODE_PRIVATE);

		stepsRestant = sharedPref.getInt(mContext.getString(R.string.g_steps),
				0) - sharedPref.getInt(mContext.getString(R.string.t_steps), 0);
		caloriesRestant = sharedPref.getInt(
				mContext.getString(R.string.t_calories), 0)
				- sharedPref.getInt(mContext.getString(R.string.t_calories), 0);

		// Si les objectifs sont d�ja atteint, met les variables � z�ro
		if (stepsRestant < 0)
			stepsRestant = 0;

		if (caloriesRestant < 0)
			caloriesRestant = 0;
	}
}