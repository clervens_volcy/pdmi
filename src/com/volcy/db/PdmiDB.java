package com.volcy.db;

import com.volcy.db.SQLiteOpenHelper.PdmiSessionSQLiteHelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;


public class PdmiDB extends SQLiteOpenHelper {
	public static final String DB_FILENAME = "pdmi.db";
	
	private Context context;
	private CursorFactory factory;
	private int version;
	
	public PdmiDB(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		
		this.context = context;
		this.factory = factory;
		this.version = version;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		(new PdmiSessionSQLiteHelper(context, DB_FILENAME, factory, version)).onCreate(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		(new PdmiSessionSQLiteHelper(context, DB_FILENAME, factory, version)).onUpgrade(db, oldVersion, newVersion);
	}

}
