package com.volcy.db.SQLiteOpenHelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/*
 * Gestion des Sessions dans Pdmi.  
 */
public class PdmiSessionSQLiteHelper extends SQLiteOpenHelper {
	private Context context;
	private CursorFactory factory;
	private int version;

	public final static String TABLE_NAME = "sessions";
	public final static String COL_ID = "id";
	public final static String COL_DATE = "date";
	public final static String COL_DISTANCE = "distance";
	public final static String COL_CALORIES = "calories";
	public final static String COL_STEPS = "steps";
	public final static String COL_DURATION = "duration";

	private final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_NAME + " (" + COL_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_DATE
			+ " INTEGER NOT NULL DEFAULT (strftime('%s','now')), "
			+ COL_DISTANCE + " REAL NOT NULL, " + COL_CALORIES
			+ " INTEGER NOT NULL, " + COL_STEPS + " INTEGER NOT NULL, "
			+ COL_DURATION + " INTEGER NOT NULL);";

	public PdmiSessionSQLiteHelper(Context context, String name,
			CursorFactory factory, int version) {
		super(context, name, factory, version);

		this.context = context;
		this.factory = factory;
		this.version = version;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// N/A, pas d'upgrade pour cette table.

		/*
		 * db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME + ";"); onCreate(db);
		 */
	}

}
