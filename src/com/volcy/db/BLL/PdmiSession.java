package com.volcy.db.BLL;

public class PdmiSession {
	private int _id;
	private long _date;
	private double _distance;
	private int _calories;
	private int _steps;
	private int _duration;

	public PdmiSession(){
		
	}
	
	public PdmiSession(int id, long date, double distance, int calories, int steps, int duration){
		_id = id;
		_date = date;
		_distance = distance;
		_calories = calories;
		_steps = steps;
		_duration = duration;
	}
	
	public PdmiSession(long date, double distance, int calories, int steps, int duration){
		_date = date;
		_distance = distance;
		_calories = calories;
		_steps = steps;
		_duration = duration;
	}

	public int get_id() {
		return _id;
	}

	public long get_date() {
		return _date;
	}

	public double get_distance() {
		return _distance;
	}

	public int get_calories() {
		return _calories;
	}

	public int get_steps() {
		return _steps;
	}

	public int get_duration() {
		return _duration;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public void set_date(long _date) {
		this._date = _date;
	}

	public void set_distance(double _distance) {
		this._distance = _distance;
	}

	public void set_calories(int _calories) {
		this._calories = _calories;
	}

	public void set_steps(int _steps) {
		this._steps = _steps;
	}

	public void set_duration(int _duration) {
		this._duration = _duration;
	}
}
