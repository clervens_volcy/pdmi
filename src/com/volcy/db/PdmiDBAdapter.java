package com.volcy.db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.volcy.db.BLL.PdmiSession;
import com.volcy.db.SQLiteOpenHelper.PdmiSessionSQLiteHelper;

public class PdmiDBAdapter {

	private PdmiDB dbHelper;
	private SQLiteDatabase myDB;

	static final int BASE_VERSION = 1;

	public PdmiDBAdapter(Context context) {
		dbHelper = new PdmiDB(context, PdmiDB.DB_FILENAME, null, BASE_VERSION);
	}

	public SQLiteDatabase open() {
		myDB = dbHelper.getWritableDatabase();
		return myDB;
	}

	public void close() {
		myDB.close();
	}

	public SQLiteDatabase getDatabase() {
		return myDB;
	}

	// PdmiSession

	public PdmiSession getSession(int index) {
		Cursor c = myDB.query(PdmiSessionSQLiteHelper.TABLE_NAME, null,
				PdmiSessionSQLiteHelper.COL_ID + " = " + index, null, null,
				null, null);

		return CursorToSessionConverter(c);
	}

	public ArrayList<PdmiSession> getSessions() {
		Cursor c = myDB.query(PdmiSessionSQLiteHelper.TABLE_NAME, null, null, null, null,
				null, null);

		return CursorToSessionsConverter(c);
	}

	public long insertSession(PdmiSession ps) {
		ContentValues valeurs = new ContentValues();

		valeurs.put(PdmiSessionSQLiteHelper.COL_CALORIES, ps.get_calories());
		valeurs.put(PdmiSessionSQLiteHelper.COL_DATE, ps.get_date());
		valeurs.put(PdmiSessionSQLiteHelper.COL_DISTANCE, ps.get_distance());
		valeurs.put(PdmiSessionSQLiteHelper.COL_DURATION, ps.get_duration());
		valeurs.put(PdmiSessionSQLiteHelper.COL_STEPS, ps.get_steps());

		return myDB.insert(PdmiSessionSQLiteHelper.TABLE_NAME, null, valeurs);
	}

	private PdmiSession CursorToSessionConverter(Cursor c) {

		if (c.getCount() == 0) {
			return null;
		}

		PdmiSession s = new PdmiSession();

		c.moveToFirst();
		do {
			s.set_id(c.getInt(0));
			s.set_date(c.getInt(1));
			s.set_distance(c.getInt(2));
			s.set_calories(c.getInt(3));
			s.set_steps(c.getInt(4));
			s.set_duration(c.getInt(5));
			
		} while (c.moveToNext());

		c.close();

		return s;
	}

	private ArrayList<PdmiSession> CursorToSessionsConverter(Cursor c) {
		if (c.getCount() == 0) {
			return null;
		}

		ArrayList<PdmiSession> sessions = new ArrayList<PdmiSession>();
		PdmiSession s;

		c.moveToFirst();
		do {
			s = new PdmiSession();
			
			s.set_id(c.getInt(0));
			s.set_date(c.getInt(1));
			s.set_distance(c.getInt(2));
			s.set_calories(c.getInt(3));
			s.set_steps(c.getInt(4));
			s.set_duration(c.getInt(5));
			
			sessions.add(s);
			
		} while (c.moveToNext());

		c.close();

		return sessions;
	}
}
